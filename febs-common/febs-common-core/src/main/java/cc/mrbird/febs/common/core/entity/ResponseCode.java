package cc.mrbird.febs.common.core.entity;

public enum ResponseCode {
    SUCCESS(1, "成功"),
    WRONG(0, "操作失败"),
    UNAUTH(401, "未授权"),
    HYSTRIX_TIME_OUT(-1, "未授权");

    private int code;
    private String info;

    private ResponseCode(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return this.code;
    }

    public String getInfo() {
        return this.info;
    }
}

package cc.mrbird.febs.common.core.entity;

import java.util.HashMap;

/**
 * @author MrBird
 */
public class FebsResponse extends HashMap<String, Object> {

    private static final long serialVersionUID = -8713837118340960775L;

    public FebsResponse code(Integer code) {
        this.put("code", code);
        return this;
    }

    public FebsResponse message(String message) {
        this.put("message", message);
        return this;
    }

    public FebsResponse data(Object data) {
        this.put("data", data);
        return this;
    }

    public FebsResponse success() {
        this.put("code", ResponseCode.SUCCESS.getCode());
        return this;
    }

    public FebsResponse success(Object data) {
        this.put("code", ResponseCode.SUCCESS.getCode());
        this.put("data", data);
        return this;
    }

    public FebsResponse wrong(String msg) {
        this.put("code", ResponseCode.WRONG.getCode());
        this.put("message", msg);
        return this;
    }

    public FebsResponse unAuth() {
        this.put("code", ResponseCode.UNAUTH.getCode());
        this.put("message", "登录授权过期，请重新登陆");
        return this;
    }

    @Override
    public FebsResponse put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public String getMessage() {
        return String.valueOf(get("message"));
    }

    public Object getData() {
        return get("data");
    }

    public Integer getCode() {
        return Integer.valueOf(get("code").toString());
    }
}

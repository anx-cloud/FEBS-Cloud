<template>
  <el-dialog :title="title" :width="width" top="50px" :close-on-click-modal="false" :close-on-press-escape="false"
    :visible.sync="isVisible">
    <el-form ref="form" :model="${className?uncap_first}" :rules="rules" label-position="right" label-width="100px">
      
	   <#list columns as column>
		  <#if column.name != 'id'>
			  <el-form-item :label="$t('table.${className?uncap_first}.${column.field?uncap_first}')" prop="${column.field?uncap_first}">
				<el-input v-model="${className?uncap_first}.${column.field?uncap_first}"  />
			  </el-form-item>
          </#if>
		  
        </#list>      
	</el-form>
    <div slot="footer" class="dialog-footer">
      <el-button type="warning" plain :loading="buttonLoading" @click="isVisible = false">
        {{ $t('common.cancel') }}
      </el-button>
      <el-button type="primary" plain :loading="buttonLoading" @click="submitForm">
        {{ $t('common.confirm') }}
      </el-button>
    </div>
  </el-dialog>
</template>
<script>
  import { validMobile } from '@/utils/my-validate'
  import Treeselect from '@riophae/vue-treeselect'
  import '@riophae/vue-treeselect/dist/vue-treeselect.css'

  export default {
    name: '${className?uncap_first}Edit',
    components: { Treeselect },
    props: {
      dialogVisible: {
        type: Boolean,
        default: false
      },
      title: {
        type: String,
        default: ''
      }
    },
    data() {
      return {
        initFlag: false,
        ${className?uncap_first}: this.init${className?uncap_first}(),
        buttonLoading: false,
        screenWidth: 0,
        width: this.initWidth()
      }
    },
    computed: {
      isVisible: {
        get() {
          return this.dialogVisible
        },
        set() {
          this.close()
          this.reset()
        }
      }
    },
    mounted() {
      window.onresize = () => {
        return (() => {
          this.width = this.initWidth()
        })()
      }
    },
    methods: {
      init${className?uncap_first}() {
        return {
          <#list columns as column>
		  ${column.field?uncap_first}: '',
		  </#list> 
        }
      },
      initWidth() {
        this.screenWidth = document.body.clientWidth
        if (this.screenWidth < 991) {
          return '90%'
        } else if (this.screenWidth < 1400) {
          return '45%'
        } else {
          return '800px'
        }
      },

      set${className?uncap_first}(val) {
        this.${className?uncap_first} = { ...val }
      },
      close() {
        this.$emit('close')
      },
      submitForm() {
        this.$refs.form.validate((valid) => {
          if (valid) {
            this.buttonLoading = true

            if (!this.${className?uncap_first}.id) {
              // create
              this.$post('${baseUrl}/${className?uncap_first}', { ...this.${className?uncap_first} }).then(() => {
                this.buttonLoading = false
                this.isVisible = false
                this.$message({
                  message: this.$t('tips.createSuccess'),
                  type: 'success'
                })
                this.$emit('success')
              })
            } else {
              // update

              this.$put('${baseUrl}/${className?uncap_first}', { ...this.${className?uncap_first} }).then(() => {
                this.buttonLoading = false
                this.isVisible = false
                
                this.$message({
                  message: this.$t('tips.updateSuccess'),
                  type: 'success'
                })
                this.$emit('success')
              })
            }
          } else {
            return false
          }
        })
      },
      reset() {
        // 先清除校验，再清除表单，不然有奇怪的bug
        this.$refs.form.clearValidate()
        this.$refs.form.resetFields()
        this.${className?uncap_first} = this.init${className?uncap_first}()
      }
    }
  }
</script>
<style lang="scss" scoped>
</style>
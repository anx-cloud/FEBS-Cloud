<template>
  <div class="app-container">
    <div class="filter-container">
      <el-input v-model="queryParams.name" :placeholder="$t('table.${className?uncap_first}.name')" class="filter-item search-item" />

      <el-date-picker v-model="queryParams.timeRange" :range-separator="null"
        :start-placeholder="$t('table.${className?uncap_first}.createTime')" value-format="yyyy-MM-dd"
        class="filter-item search-item date-range-item" type="daterange" />
      <el-button class="filter-item" type="primary" @click="search">
        {{ $t('table.search') }}
      </el-button>
      <el-button class="filter-item" type="success" @click="reset">
        {{ $t('table.reset') }}
      </el-button>
      <el-dropdown v-has-any-permission="['${className?uncap_first}:add','${className?uncap_first}:delete']" trigger="click" class="filter-item">
        <el-button>
          {{ $t('table.more') }}<i class="el-icon-arrow-down el-icon--right" />
        </el-button>
        <el-dropdown-menu slot="dropdown">
          <el-dropdown-item v-has-permission="['${className?uncap_first}:add']" @click.native="add">{{ $t('table.add') }}</el-dropdown-item>
          <el-dropdown-item v-has-permission="['${className?uncap_first}:delete']" @click.native="batchDelete">{{ $t('table.delete') }}
          </el-dropdown-item>

        </el-dropdown-menu>
      </el-dropdown>
    </div>

    <el-table ref="table" :key="tableKey" v-loading="loading" :data="list" border fit style="width: 100%;"
      @selection-change="onSelectChange" @sort-change="sortChange">
      <el-table-column type="selection" align="center" width="40px" />
      
	  <#list columns as column>
		 
		  <el-table-column :label="$t('table.${className?uncap_first}.${column.field?uncap_first}')" class-name="status-col">
			<template slot-scope="{row}">
			  <span>
				{{ row.${column.field?uncap_first} }}
			  </span>
			</template>
		  </el-table-column>
      </#list>  


      <el-table-column :label="$t('table.operation')" align="center" min-width="150px"
        class-name="small-padding fixed-width">
        <template slot-scope="{row}">
          <i v-hasPermission="['${className?uncap_first}:list']" class="el-icon-view table-operation" style="color: #87d068;"
            @click="view(row)" />
          <i v-hasPermission="['${className?uncap_first}:update']" class="el-icon-setting table-operation" style="color: #2db7f5;"
            @click="edit(row)" />
          <i v-hasPermission="['${className?uncap_first}:delete']" class="el-icon-delete table-operation" style="color: #f50;"
            @click="singleDelete(row)" />
          <el-link v-has-no-permission="['${className?uncap_first}:list','${className?uncap_first}:update','${className?uncap_first}:delete']" class="no-perm">
            {{ $t('tips.noPermission') }}
          </el-link>
        </template>
      </el-table-column>


    </el-table>
    <pagination v-show="total>0" :total="total" :page.sync="pagination.num" :limit.sync="pagination.size"
      @pagination="search" />
    <${className?uncap_first}-edit ref="edit" :dialog-visible="dialog.isVisible" :title="dialog.title" @success="editSuccess"
      @close="editClose" />
    <${className?uncap_first}-view ref="view" :dialog-visible="${className?uncap_first}ViewVisible" @close="viewClose" />
  </div>
</template>

<script>
  import Pagination from '@/components/Pagination'
  import ${className?uncap_first}Edit from './Edit'
  import ${className?uncap_first}View from './View'

  export default {
    name: '${className?uncap_first}Manage',
    components: { Pagination, ${className?uncap_first}Edit, ${className?uncap_first}View },
    filters: {
      sexFilter(status) {
        const map = {
          0: 'success',
          1: 'danger',
          2: 'info'
        }
        return map[status]
      },
      statusFilter(status) {
        const map = {
          0: 'danger',
          1: 'success'
        }
        return map[status]
      }
    },
    data() {
      return {
        dialog: {
          isVisible: false,
          title: ''
        },
        ${className?uncap_first}ViewVisible: false,
        tableKey: 0,
        loading: false,
        list: null,
        total: 0,
        queryParams: {},
        sort: {},
        selection: [],
        pagination: {
          size: 10,
          num: 1
        }
      }
    },
    computed: {
      
    },
    mounted() {
      this.fetch()
    },
    methods: {

      viewClose() {
        this.${className?uncap_first}ViewVisible = false
      },
      editClose() {
        this.dialog.isVisible = false
      },
      editSuccess() {
        this.search()
      },
      onSelectChange(selection) {
        this.selection = selection
      },
      search() {
        this.fetch({
          ...this.queryParams,
          ...this.sort
        })
      },
      reset() {
        this.queryParams = {}
        this.sort = {}
        this.$refs.table.clearSort()
        this.$refs.table.clearFilter()
        this.search()
      },
      exportExcel() {
        this.$download('${baseUrl}/${className?uncap_first}/excel', {
          pageSize: this.pagination.size,
          pageNum: this.pagination.num,
          ...this.queryParams
        }, `${className?uncap_first}_${new Date().getTime()}.xlsx`)
      },
      add() {
        this.dialog.title = this.$t('common.add')
        this.dialog.isVisible = true
      },
      singleDelete(row) {
        
        this.$refs.table.toggleRowSelection(row, true)
        this.batchDelete()
      },
      batchDelete() {
        
        if (!this.selection.length) {
          this.$message({
            message: this.$t('tips.noDataSelected'),
            type: 'warning'
          })
          return
        }
        let contain = false
        this.$confirm(this.$t('tips.confirmDelete'), this.$t('common.tips'), {
          confirmButtonText: this.$t('common.confirm'),
          cancelButtonText: this.$t('common.cancel'),
          type: 'warning'
        }).then(() => {
          const ${className?uncap_first}Ids = []
          this.selection.forEach((u) => {            
            ${className?uncap_first}Ids.push(u.id)
          })
          
          this.delete(${className?uncap_first}Ids)
          
        }).catch(() => {
          this.clearSelections()
        })
      },
      clearSelections() {
        this.$refs.table.clearSelection()
      },
      delete(${className?uncap_first}Ids) {
        this.loading = true
        this.$delete(`${baseUrl}/${className?uncap_first}/${${className?uncap_first}Ids}`).then(() => {
          this.$message({
            message: this.$t('tips.deleteSuccess'),
            type: 'success'
          })
          this.search()
        })
      },
      view(row) {
        this.$refs.view.set${className?uncap_first}(row)
        this.${className?uncap_first}ViewVisible = true
      },
      edit(row) {
        let roleId = []
        this.$get(`${baseUrl}/${className?uncap_first}/${row.id}`).then((r) => {
          this.$refs.edit.set${className?uncap_first}(row)
          this.dialog.title = this.$t('common.edit')
          this.dialog.isVisible = true
        })
      },
      fetch(params = {}) {
        params.pageSize = this.pagination.size
        params.pageNum = this.pagination.num
        if (this.queryParams.timeRange) {
          params.createTimeFrom = this.queryParams.timeRange[0]
          params.createTimeTo = this.queryParams.timeRange[1]
        }
        this.loading = true
        this.$get('${baseUrl}/${className?uncap_first}/list', {
          ...params
        }).then((r) => {
          const data = r.data.data
          this.total = data.total
          this.list = data.rows
          this.loading = false
        })
      },
      sortChange(val) {
        this.sort.field = val.prop
        this.sort.order = val.order
        this.search()
      }
    }
  }
</script>
<style lang="scss" scoped>
</style>
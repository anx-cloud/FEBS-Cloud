<template>
  <el-dialog :title="$t('common.view')" :width="width" :visible.sync="isVisible" class="${className?uncap_first}-view">
    <el-row :gutter="10">
      <el-col :xs="24" :sm="24">
        <div class="img-wrapper">
          <img :src="avatar">
        </div>
      </el-col>
    </el-row>
	
	<#list columns as column>
		<el-row :gutter="10">
		  <el-col :xs="24" :sm="12">
			<div class="view-item">
			  <i class="el-icon-${className?uncap_first}" /> <span>{{ $t('table.${className?uncap_first}.${column.field?uncap_first}') +'：' }}</span> {{ ${className?uncap_first}.${column.field?uncap_first} }}
			</div>
		  </el-col>

		</el-row>
		<el-row :gutter="10">

		</el-row>
		
    </#list> 
  </el-dialog>
</template>
<script>
  export default {
    name: '${className?uncap_first}View',
    props: {
      dialogVisible: {
        type: Boolean,
        default: false
      }
    },
    data() {
      return {
        screenWidth: 0,
        width: this.initWidth(),
        ${className?uncap_first}: {}
      }
    },
    computed: {
      isVisible: {
        get() {
          return this.dialogVisible
        },
        set() {
          this.close()
        }
      },
      avatar() {
        if (this.${className?uncap_first}.avatar) {
          return require(`@/assets/avatar/${this.${className?uncap_first}.avatar}`)
        } else {
          return require('@/assets/avatar/default.jpg')
        }
      }
    },
    mounted() {
      window.onresize = () => {
        return (() => {
          this.width = this.initWidth()
        })()
      }
    },
    methods: {
      initWidth() {
        this.screenWidth = document.body.clientWidth
        if (this.screenWidth < 550) {
          return '95%'
        } else if (this.screenWidth < 990) {
          return '580px'
        } else if (this.screenWidth < 1400) {
          return '600px'
        } else {
          return '650px'
        }
      },
      set${className?uncap_first}(val) {
        this.${className?uncap_first} = { ...val }
      },
      close() {
        this.$emit('close')
      }
    }
  }
</script>
<style lang="scss" scoped>
  .${className?uncap_first}-view {
    .img-wrapper {
      text-align: center;
      margin-top: -1.5rem;
      margin-bottom: 10px;

      img {
        width: 4rem;
        border-radius: 50%;
      }
    }

    .view-item {
      margin: 7px;

      i {
        font-size: .97rem;
      }

      span {
        margin-left: 5px;
      }
    }
  }
</style>

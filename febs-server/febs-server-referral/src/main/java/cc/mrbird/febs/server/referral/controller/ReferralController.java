package cc.mrbird.febs.server.referral.controller;

import cc.mrbird.febs.common.core.utils.DateUtil;
import cc.mrbird.febs.server.referral.entity.Referral;
import cc.mrbird.febs.server.referral.service.IReferralService;
import cc.mrbird.febs.common.core.entity.FebsResponse;
import cc.mrbird.febs.common.core.entity.QueryRequest;
import cc.mrbird.febs.common.core.exception.FebsException;
import cc.mrbird.febs.common.core.utils.FebsUtil;
import cc.mrbird.febs.common.core.utils.HttpClientUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * 转诊列表 Controller
 *
 * @author LiuDong
 * @date 2021-03-02 17:47:12
 */
@Slf4j
@Validated
@RestController
@RequestMapping("referral")
@RequiredArgsConstructor
public class ReferralController {

    private final IReferralService referralService;

    @GetMapping
//    @PreAuthorize("hasAuthority('referral:list')")
    public FebsResponse getAllReferrals(Referral referral) {
        return new FebsResponse().success(referralService.findReferrals(referral));
    }

    @GetMapping("list")
//    @PreAuthorize("hasAuthority('referral:list')")
    public FebsResponse referralList(QueryRequest request, Referral referral) {
        Map<String, Object> dataTable = FebsUtil.getDataTable(this.referralService.findReferrals(request, referral));
        return new FebsResponse().success(dataTable);
    }
    @GetMapping("backout")
    public FebsResponse backout(String  id ) {
        Referral referralItem = referralService.getById(id);
        String applyStatus = referralItem.getApplyStatus();
        if ("0".equals(applyStatus) || "1".equals(applyStatus)){
            referralService.removeById(id);
            return new FebsResponse().success().message("删除成功");
        }else {
            return new FebsResponse().wrong("当前状态不可撤销");
        }
    }

    @PostMapping
//    @PreAuthorize("hasAuthority('referral:add')")
    public FebsResponse addReferral(@RequestBody Referral referral) throws FebsException {
        try {
            FebsResponse febsResponse = this.referralService.createReferral(referral);
            return febsResponse;
        } catch (Exception e) {
            String message = "新增Referral失败";
            log.error(message, e);
            throw new FebsException(message);
        }
    }

	@PostMapping("sendsms")
	public FebsResponse sendRefSMS(@RequestBody HashMap<String, String> list) throws FebsException {
        try {
			String url = "http://192.160.161.71:8899/sms-send/send";
			String mobile = list.get("mobile");
			String content = list.get("content");

			if(mobile == null) {
			   throw new FebsException("手机号是空");
			}

	        if(content == null || content.length() < 1) {
			   content = "已发送转诊申请";
			}

			HashMap<String, Object> hmp = new HashMap<String, Object>();
			hmp.put("sendPhone", mobile);
			hmp.put("sendContent", content);
			hmp.put("systemCode", "L1016");
			HashMap<String, Object> hmpd = new HashMap<String, Object>();
			hmpd.put("data", hmp);

			// 短信接口必须双引号拼接传参
			String input_ = "{\"sendPhone\":\"" + mobile + "\", \"sendContent\":\"" + content +"\",\"systemCode\":\"L1016\"}";
			String ret2 = HttpClientUtil.post(url, input_);

            return new FebsResponse().success(ret2);
        } catch (Exception e) {
            String message = "发送短信失败";
            log.error(message, e);
            throw new FebsException(message);
        }
    }

    @DeleteMapping
//    @PreAuthorize("hasAuthority('referral:delete')")
    public FebsResponse deleteReferral(Referral referral) throws FebsException {
        try {
            this.referralService.deleteReferral(referral);
            return new FebsResponse().success();
        } catch (Exception e) {
            String message = "删除Referral失败";
            log.error(message, e);
            throw new FebsException(message);
        }
    }

    @PutMapping
//    @PreAuthorize("hasAuthority('referral:update')")
    public FebsResponse updateReferral(@RequestBody Referral referral) throws FebsException {
        log.info("============================={}", JSON.toJSONString(referral));
        //接收
        String nwoStr = DateUtil.getDateFormat(new Date(), "yyyy-MM-dd HH:mm:ss");
        try {
            String applyStatus = referral.getApplyStatus();
            //  接收时间
            if (StringUtils.isNotBlank(applyStatus) && "2".equals(applyStatus)){
                referral.setConfirmTransferDate(nwoStr);
            }
            FebsResponse febsResponse = this.referralService.updateReferral(referral);
            return febsResponse;
        } catch (Exception e) {
            String message = "修改Referral失败";
            log.error(message, e);
            throw new FebsException(message);
        }
    }
}

package cc.mrbird.febs.server.referral;

import cc.mrbird.febs.common.security.starter.annotation.EnableFebsCloudResourceServer;
import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableFeignClients
@SpringBootApplication
//@EnableTransactionManagement
@EnableFebsCloudResourceServer
//@EnableDistributedTransaction
@MapperScan("cc.mrbird.febs.server.referral.mapper")
public class FebsServerReferralApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(FebsServerReferralApplication.class)
                .web(WebApplicationType.SERVLET)
                .run(args);
    }

}

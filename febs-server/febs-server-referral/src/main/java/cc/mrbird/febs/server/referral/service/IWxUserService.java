package cc.mrbird.febs.server.referral.service;


import cc.mrbird.febs.common.core.entity.FebsResponse;
import cc.mrbird.febs.common.core.entity.QueryRequest;
import cc.mrbird.febs.server.referral.dto.WxUserDTO;
import cc.mrbird.febs.server.referral.entity.WxUser;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 微信用户信息表 Service接口
 *
 * @author MrBird
 * @date 2021-02-24 10:39:20
 */
public interface IWxUserService extends IService<WxUser> {

    FebsResponse saveWxUser(WxUserDTO wxUserDTO);

    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param wxUser wxUser
     * @return IPage<WxUser>
     */
    IPage<WxUser> findWxUsers(QueryRequest request, WxUser wxUser);

    /**
     * 查询（所有）
     *
     * @param wxUser wxUser
     * @return List<WxUser>
     */
    List<WxUser> findWxUsers(WxUser wxUser);

    /**
     * 新增
     *
     * @param wxUser wxUser
     */
    void createWxUser(WxUser wxUser);

    /**
     * 修改
     *
     * @param wxUser wxUser
     */
    void updateWxUser(WxUser wxUser);

    /**
     * 删除
     *
     * @param wxUser wxUser
     */
    void deleteWxUser(WxUser wxUser);
}

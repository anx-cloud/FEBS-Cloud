package cc.mrbird.febs.server.referral.entity;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
* 医生信息 Entity
*
* @author LiuDong
* @date 2021-03-02 10:21:52
*/
@Data
@TableName("t_doctor")
public class Doctor {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty("主键")
    private String id;

    /**
     * 机构名称
     */
    @TableField("organization")
    @ApiModelProperty("机构名称")
    private String organization;

    /**
     * 机构ID
     */
    @TableField("organization_id")
    @ApiModelProperty("机构ID")
    private String organizationId;

    /**
     * 科室ID
     */
    @TableField("clinic_id")
    @ApiModelProperty("科室ID")
    private String clinicId;

    /**
     * 科室名称
     */
    @TableField("clinic_name")
    @ApiModelProperty("科室名称")
    private String clinicName;

    /**
     * 医生姓名
     */
    @TableField("doctor_name")
    @ApiModelProperty("医生姓名")
    private String doctorName;

    /**
     * 医生ID（工号）
     */
    @TableField("doctor_id")
    @ApiModelProperty("医生ID（工号）")
    private String doctorId;

    /**
     * 性别 01-男，02-女，03-未知
     */
    @TableField("gender")
    @ApiModelProperty("性别 01-男，02-女，03-未知")
    private String gender;

    /**
     * 身份证号
     */
    @TableField("id_no")
    @ApiModelProperty("身份证号")
    private String idNo;

    /**
     * 手机号
     */
    @TableField("mobile")
    @ApiModelProperty("手机号")
    private String mobile;

    /**
     * 邮箱
     */
    @TableField("email")
    @ApiModelProperty("邮箱")
    private String email;

    /**
     * 微信用户ID
     */
    @TableField("wx_user_id")
    @ApiModelProperty("微信用户ID")
    private String wxUserId;

    /**
     * 地址
     */
    @TableField("address")
    @ApiModelProperty("地址")
    private String address;

    /**
     * 短信验证码
     */
    @TableField("sms_code")
    @ApiModelProperty("短信验证码")
    private String smsCode;

    /**
     * 数据添加用户ID
     */
    @TableField("create_by")
    @ApiModelProperty("数据添加用户ID")
    private String createBy;

    /**
     * 数据添加时间
     */
    @TableField("create_at")
    @ApiModelProperty("数据添加时间")
    private Date createAt;

    /**
     * 数据修改用户ID
     */
    @TableField("update_by")
    @ApiModelProperty("数据修改用户ID")
    private String updateBy;

    /**
     * 数据修改时间
     */
    @TableField("udpate_at")
    @ApiModelProperty("数据修改时间")
    private Date udpateAt;

    /**
     * 备注
     */
    @TableField("remark")
    @ApiModelProperty("备注")
    private String remark;
	
	/**
     * 备注
     */
    @TableField("ranks")
    @ApiModelProperty("职称")
    private String ranks;
	
    /**
     * 权重
     */
    @TableField("rank")
    @ApiModelProperty("权重")
    private Integer rank;

    /**
     * 0未删除；1删除
     */
    @TableField("delete_at")
    @ApiModelProperty(" 0未删除；1删除")
    private Integer deleteAt;

    /**
     * 状态 0未审核；1已审核
     */
    @TableField("status")
    @ApiModelProperty("状态 0未审核；1已审核")
    private Integer status;

}
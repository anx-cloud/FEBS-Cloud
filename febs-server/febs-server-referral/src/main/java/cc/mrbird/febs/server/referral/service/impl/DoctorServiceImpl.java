package cc.mrbird.febs.server.referral.service.impl;

import cc.mrbird.febs.common.core.entity.FebsResponse;
import cc.mrbird.febs.server.referral.entity.Doctor;
import cc.mrbird.febs.server.referral.entity.Referral;
import cc.mrbird.febs.server.referral.mapper.DoctorMapper;
import cc.mrbird.febs.server.referral.service.IDoctorService;
import cc.mrbird.febs.common.core.entity.ResponseCode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cc.mrbird.febs.common.core.entity.QueryRequest;

import javax.print.Doc;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * 医生信息 Service实现
 *
 * @author LiuDong
 * @date 2021-03-02 10:21:52
 */
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class DoctorServiceImpl extends ServiceImpl<DoctorMapper, Doctor> implements IDoctorService {

    private final DoctorMapper doctorMapper;

    @Override
    public int updateStatus(String id) {
        Doctor doctor = this.doctorMapper.selectById(id);
        if(null == doctor){
            return 0;
        }
        Integer status = (doctor.getStatus() == 0) ? 1 : 0;
        HashMap hashMap = new HashMap();
        hashMap.put("status", status);
        hashMap.put("id", id);
        return this.doctorMapper.updateStatus(hashMap);
    }

    @Override
    public IPage<Doctor> findDoctors(QueryRequest request, Doctor doctor) {
        LambdaQueryWrapper<Doctor> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        if(StringUtils.isNotBlank(doctor.getDoctorId()))
            queryWrapper.eq(Doctor::getDoctorId, doctor.getDoctorId());
        if(StringUtils.isNotBlank(doctor.getDoctorName()))
            queryWrapper.eq(Doctor::getDoctorName, doctor.getDoctorName());
        if(null != doctor.getStatus())
            queryWrapper.eq(Doctor::getStatus, doctor.getStatus());
        if(null != doctor.getOrganizationId())
            queryWrapper.eq(Doctor::getOrganizationId, doctor.getOrganizationId());
        if(null != doctor.getClinicId())
            queryWrapper.eq(Doctor::getClinicId, doctor.getClinicId());
        Page<Doctor> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.page(page, queryWrapper);
    }

    @Override
    public List<Doctor> findDoctors(Doctor doctor) {
        LambdaQueryWrapper<Doctor> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        if(StringUtils.isNotBlank(doctor.getDoctorId()))
            queryWrapper.eq(Doctor::getDoctorId, doctor.getDoctorId());
        if(StringUtils.isNotBlank(doctor.getDoctorName()))
            queryWrapper.eq(Doctor::getDoctorName, doctor.getDoctorName());
        if(null != doctor.getStatus())
            queryWrapper.eq(Doctor::getStatus, doctor.getStatus());
        if(null != doctor.getOrganizationId())
            queryWrapper.eq(Doctor::getOrganizationId, doctor.getOrganizationId());
        if(null != doctor.getClinicId())
            queryWrapper.eq(Doctor::getClinicId, doctor.getClinicId());
        return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public FebsResponse createDoctor(Doctor doctor) {
        FebsResponse febsResponse = new FebsResponse();
        String doctorId = doctor.getDoctorId();
        LambdaQueryWrapper<Doctor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Doctor::getDoctorId, doctorId);
        List<Doctor> doctorList = doctorMapper.selectList(queryWrapper);
        if(doctorList.size() > 0){
            febsResponse.code(ResponseCode.WRONG.getCode());
            febsResponse.data(doctorList);
            febsResponse.message("已注册过该医生");
        }
        doctor.setId(UUID.randomUUID().toString());
        doctor.setStatus(0);
        doctor.setCreateAt(new Date());
        this.save(doctor);
        febsResponse.success(doctor);
        return febsResponse;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public FebsResponse updateDoctor(Doctor doctor) {
        this.saveOrUpdate(doctor);
        return new FebsResponse().success(doctor);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteDoctor(Doctor doctor) {
        LambdaQueryWrapper<Doctor> wapper = new LambdaQueryWrapper<>();
        // TODO 设置删除条件
        wapper.eq(Doctor::getId, doctor.getId());
        this.remove(wapper);
    }
}

package cc.mrbird.febs.server.referral.service;

import cc.mrbird.febs.common.core.entity.FebsResponse;
import cc.mrbird.febs.server.referral.entity.Doctor;

import cc.mrbird.febs.common.core.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 医生信息 Service接口
 *
 * @author LiuDong
 * @date 2021-03-02 10:21:52
 */
public interface IDoctorService extends IService<Doctor> {

    /**
     * 修改状态
     */
    int updateStatus(String id);

    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param doctor doctor
     * @return IPage<Doctor>
     */
    IPage<Doctor> findDoctors(QueryRequest request, Doctor doctor);

    /**
     * 查询（所有）
     *
     * @param doctor doctor
     * @return List<Doctor>
     */
    List<Doctor> findDoctors(Doctor doctor);

    /**
     * 新增
     *
     * @param doctor doctor
     */
    FebsResponse createDoctor(Doctor doctor);

    /**
     * 修改
     *
     * @param doctor doctor
     */
    FebsResponse updateDoctor(Doctor doctor);

    /**
     * 删除
     *
     * @param doctor doctor
     */
    void deleteDoctor(Doctor doctor);
}

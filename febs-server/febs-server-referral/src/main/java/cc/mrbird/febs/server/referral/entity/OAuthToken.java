package cc.mrbird.febs.server.referral.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class OAuthToken {
    @JSONField(name = "access_token")
    private String  accessToken;
    @JSONField(name = "token_type")
    private String  tokenType;
    @JSONField(name = "refresh_token")
    private String  refreshToken;
    @JSONField(name = "expires_in")
    private String  expiresIn;
    @JSONField(name = "scope")
    private String  scope;
}

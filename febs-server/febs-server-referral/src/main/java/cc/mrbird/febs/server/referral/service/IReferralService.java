package cc.mrbird.febs.server.referral.service;

import cc.mrbird.febs.common.core.entity.FebsResponse;
import cc.mrbird.febs.server.referral.entity.Referral;

import cc.mrbird.febs.common.core.entity.QueryRequest;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 转诊列表 Service接口
 *
 * @author LiuDong
 * @date 2021-03-02 17:47:12
 */
public interface IReferralService extends IService<Referral> {
    /**
     * 查询（分页）
     *
     * @param request QueryRequest
     * @param referral referral
     * @return IPage<Referral>
     */
    IPage<Referral> findReferrals(QueryRequest request, Referral referral);

    /**
     * 查询（所有）
     *
     * @param referral referral
     * @return List<Referral>
     */
    List<Referral> findReferrals(Referral referral);

    /**
     * 新增
     *
     * @param referral referral
     */
    FebsResponse createReferral(Referral referral);

    /**
     * 修改
     *
     * @param referral referral
     */
    FebsResponse updateReferral(Referral referral);

    /**
     * 删除
     *
     * @param referral referral
     */
    void deleteReferral(Referral referral);
}

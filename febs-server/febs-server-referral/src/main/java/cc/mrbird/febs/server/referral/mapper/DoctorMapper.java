package cc.mrbird.febs.server.referral.mapper;

import cc.mrbird.febs.server.referral.entity.Doctor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * 医生信息 Mapper
 *
 * @author LiuDong
 * @date 2021-03-02 10:21:52
 */
public interface DoctorMapper extends BaseMapper<Doctor> {

    public int updateStatus(HashMap hashMap);
	
	/**
     * 查找医生信息
     */
    List<Doctor> selectDoctor(@Param("doctor") Doctor doctor);
}

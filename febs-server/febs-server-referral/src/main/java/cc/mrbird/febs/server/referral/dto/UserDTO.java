package cc.mrbird.febs.server.referral.dto;

import lombok.Data;

@Data
public class UserDTO {
    private String userId;
    private String username;
    private String password;
    private String email;
    private String mobile;
    private String sex;
    private String status;
    private String roleId;
    private String deptIds;
    private String deptIdsArr;

}
package cc.mrbird.febs.server.referral.controller;

import cc.mrbird.febs.server.referral.entity.Doctor;
import cc.mrbird.febs.server.referral.service.IDoctorService;
import cc.mrbird.febs.common.core.entity.FebsResponse;
import cc.mrbird.febs.common.core.entity.QueryRequest;
import cc.mrbird.febs.common.core.exception.FebsException;
import cc.mrbird.febs.common.core.utils.FebsUtil;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.Map;

/**
 * 医生信息 Controller
 *
 * @author LiuDong
 * @date 2021-03-02 10:21:52
 */
@Slf4j
@Validated
@RestController
@RequestMapping("doctor")
@RequiredArgsConstructor
public class DoctorController {

    private final IDoctorService doctorService;

    @GetMapping("updateState")
//    @PreAuthorize("hasAuthority('doctor:list')")
    @ApiOperation(value = "修改状态", notes = "修改状态")
    public FebsResponse updateState(@RequestParam(value = "id") String id) {
        return (this.doctorService.updateStatus(id) == 1) ? new FebsResponse().success() : new FebsResponse().wrong("状态修改");
    }

    @GetMapping
//    @PreAuthorize("hasAuthority('doctor:list')")
    public FebsResponse getAllDoctors(Doctor doctor) {
        return new FebsResponse().success(doctorService.findDoctors(doctor));
    }

    @GetMapping("list")
//    @PreAuthorize("hasAuthority('doctor:list')")
    public FebsResponse doctorList(QueryRequest request, Doctor doctor) {
        Map<String, Object> dataTable = FebsUtil.getDataTable(this.doctorService.findDoctors(request, doctor));
        return new FebsResponse().success(dataTable);
    }

    @PostMapping
//    @PreAuthorize("hasAuthority('doctor:add')")
    public FebsResponse addDoctor(@RequestBody Doctor doctor) throws FebsException {
        try {
            FebsResponse febsResponse = this.doctorService.createDoctor(doctor);
            return febsResponse;
        } catch (Exception e) {
            String message = "新增Doctor失败";
            log.error(message, e);
            throw new FebsException(message);
        }
    }

    @DeleteMapping
//    @PreAuthorize("hasAuthority('doctor:delete')")
    public FebsResponse deleteDoctor(Doctor doctor) throws FebsException {
        try {
            this.doctorService.deleteDoctor(doctor);
            return new FebsResponse().success();
        } catch (Exception e) {
            String message = "删除Doctor失败";
            log.error(message, e);
            throw new FebsException(message);
        }
    }

    @PutMapping
//    @PreAuthorize("hasAuthority('doctor:update')")
    public FebsResponse updateDoctor(@Valid Doctor doctor) throws FebsException {
        try {
            FebsResponse febsResponse = this.doctorService.updateDoctor(doctor);
            return febsResponse;
        } catch (Exception e) {
            String message = "修改Doctor失败";
            log.error(message, e);
            throw new FebsException(message);
        }
    }

    @PostMapping("update")
//    @PreAuthorize("hasAuthority('doctor:update')")
    public FebsResponse updateJsonDoctor(@RequestBody Doctor doctor) throws FebsException {
        try {
            FebsResponse febsResponse = this.doctorService.updateDoctor(doctor);
            return febsResponse;
        } catch (Exception e) {
            String message = "修改Doctor失败";
            log.error(message, e);
            throw new FebsException(message);
        }
    }
}

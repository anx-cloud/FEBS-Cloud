package cc.mrbird.febs.server.referral.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WxUserDTO {
    @ApiModelProperty("登录类型")
    private String type;
    @ApiModelProperty("手机号")
    private String mobile;//手机号
    @ApiModelProperty("短信验证码")
    private String sendCode;
    //    @NotEmpty(message = "微信凭证码不能为空")
    @ApiModelProperty("微信凭证")
    private String code;

    //微信登录相关字段
    @ApiModelProperty("encryptedData")
    private String encryptedData;
    @ApiModelProperty("iv")
    private String iv;
    @ApiModelProperty("sessionKey")
    private String sessionKey;
}
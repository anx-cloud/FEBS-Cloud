package cc.mrbird.febs.server.referral.entity;

import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
* 微信用户信息表 Entity
*
* @author MrBird
* @date 2021-02-24 10:39:20
*/
@Data
@TableName("t_wx_user")
public class WxUser {

    /**
     * 
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 用户名
     */
    @TableField("user_name")
    private String userName;

    /**
     * 密码
     */
    @TableField("pass_word")
    private String passWord;

    /**
     * 身份证号
     */
    @TableField("id_no")
    private String idNo;

    /**
     * 用户姓名
     */
    @TableField("pat_name")
    private String patName;

    /**
     * 手机号
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 性别 01-男，02-女，03-未知
     */
    @TableField("gender")
    private String gender;

    /**
     * 邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 微信用户唯一ID
     */
    @TableField("open_id")
    private String openId;

    /**
     * 加密数据
     */
    @TableField("seesion_key")
    private String seesionKey;

    /**
     * 短信验证码
     */
    @TableField("sms_code")
    private String smsCode;

    /**
     * 地址
     */
    @TableField("address")
    private String address;

    /**
     * im账号
     */
    @TableField("im_user_name")
    private String imUserName;

    /**
     * im密码
     */
    @TableField("im_pass_word")
    private String imPassWord;

    /**
     * 绑定医生ID
     */
    @TableField("doctor_id")
    private String doctorId;

}
package cc.mrbird.febs.server.referral.controller;

import cc.mrbird.febs.common.core.entity.FebsResponse;

//import cc.mrbird.febs.server.referral.dto.UserDTO;
import cc.mrbird.febs.server.referral.service.IOAuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping
@RequiredArgsConstructor
public class TestController {
//    @Value("${wx.user_system_uri}")
//    private String userSystemUri;
//    @Value("${wx.user_oauth_uri}")
//    private String userOAuthUri;

    private final IOAuthService oAuthService;

    @GetMapping("test")
    public FebsResponse test(HttpServletRequest request){
//        Map<String,Object> param = new LinkedHashMap<>();
//        param.put("grant_type", "password");
//        param.put("username", "Ivan");
//        param.put("password", "1234qwer");
//        Map<String, Object> headerParams = new LinkedHashMap<>();
//        headerParams.put("Authorization", "Basic ZmViczoxMjM0NTY=");
        // 获得Http客户端(可以理解为:你得先有一个浏览器;注意:实际上HttpClient与浏览器是不一样的)
//        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//
//        // 参数
//        StringBuffer params = new StringBuffer();
//        try {
//            // 字符数据最好encoding以下;这样一来，某些特殊字符才能传过去(如:某人的名字就是“&”,不encoding的话,传不过去)
//            params.append("grant_type=password");
//            params.append("&");
//            params.append("username=Ivan");
//            params.append("&");
//            params.append("password=1234qwer");
//        } catch (Exception e1) {
//            e1.printStackTrace();
//        }
//        String url = "http://localhost:8301/auth/oauth/token";
//        log.info("访问：" + url + ": ", params.toString());
//        HttpPost httpPost = new HttpPost(url + "?" + params);
//        httpPost.setHeader("Authorization", "Basic ZmViczoxMjM0NTY=");
//        CloseableHttpResponse response = null;
//        String result = "";
//        try {
//            // 由客户端执行(发送)Post请求
//            response = httpClient.execute(httpPost);
//            // 从响应模型中获取响应实体
//            HttpEntity responseEntity = response.getEntity();
//
//            System.out.println("响应状态为:" + response.getStatusLine());
//            if (responseEntity != null) {
//                System.out.println("响应内容长度为:" + responseEntity.getContentLength());
//                result = EntityUtils.toString(responseEntity);
//                System.out.println("响应内容为:" + result);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        finally {
//            try {
//                // 释放资源
//                if (httpClient != null) {
//                    httpClient.close();
//                }
//                if (response != null) {
//                    response.close();
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        log.info("返回结果：{}", result);
//        List<NameValuePair> params = new ArrayList<>();
//        params.add(new BasicNameValuePair("username", "Ivan"));
//        params.add(new BasicNameValuePair("password", "1234qwer"));

//        String name = request.getParameter("name");
//        AuthUtils authUtils = new AuthUtils();
//        OAuthToken oAuthToken = authUtils.getOAuthInfo(name, "1234qwer", userOAuthUri);

//        String url = userSystemUri + "/check/" + name;
//        List<HashMap<String, Object>> headerMapList = new ArrayList<>();
//        HashMap<String, Object> headerMap = new LinkedHashMap<>();
//        headerMap.put("key", "Authorization");
//        headerMap.put("value", oAuthToken.getTokenType() + " " + oAuthToken.getAccessToken());
//        headerMapList.add(headerMap);
//        List<NameValuePair> params = new ArrayList<>();
//        String result = AuthUtils.doGetFormData(url, params, headerMapList);
//        OAuthToken oAuthToken = oAuthService.getOAuthInfo("18622214927");
//        String mobile = request.getParameter("mobile");
//        String username = "wx" + mobile;
//        boolean canAdd = oAuthService.checkUsername(username);
//        if(canAdd){
//            UserDTO userDTO = new UserDTO();
//            userDTO.setUsername(username);
//            userDTO.setPassword("1234qwer");
//            userDTO.setSex("0");
//            userDTO.setMobile(mobile);
//            userDTO.setStatus("1");
//            userDTO.setRoleId("1");
//            oAuthService.addSysUser(userDTO);
//        }
        FebsResponse febsResponse = new FebsResponse();
        febsResponse.put("test", "febs-server-referral ;)");
//        febsResponse.put("oAuthToken", oAuthToken);
//        febsResponse.put("check", result);
//        febsResponse.put("msg", canAdd ? "已添加" : "已查询");
        return febsResponse;
    }
}
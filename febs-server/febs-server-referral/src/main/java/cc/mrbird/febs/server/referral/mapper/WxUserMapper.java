package cc.mrbird.febs.server.referral.mapper;

import cc.mrbird.febs.server.referral.entity.WxUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 微信用户信息表 Mapper
 *
 * @author MrBird
 * @date 2021-02-24 10:39:20
 */
public interface WxUserMapper extends BaseMapper<WxUser> {

}

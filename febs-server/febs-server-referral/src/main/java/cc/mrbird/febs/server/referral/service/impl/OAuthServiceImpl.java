package cc.mrbird.febs.server.referral.service.impl;

import cc.mrbird.febs.common.core.entity.FebsResponse;
import cc.mrbird.febs.server.referral.dto.UserDTO;
import cc.mrbird.febs.server.referral.entity.OAuthToken;
import cc.mrbird.febs.server.referral.service.IOAuthService;
import cc.mrbird.febs.server.referral.utils.AuthUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * 微信用户信息表 Service实现
 *
 * @author MrBird
 * @date 2021-02-24 10:39:20
 */
@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class OAuthServiceImpl implements IOAuthService {

    @Value("${wx.appid}")
    private String  appid;
    @Value("${wx.user_system_uri}")
    private String userSystemUri;
    @Value("${wx.user_oauth_uri}")
    private String userOAuthUri;
    @Value("${wx.user_register}")
    private String userRegister;
    @Value("${wx.user_register_password}")
    private String userRegisterPassword;


    /**
     * 获取手机令牌
     * @param username
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public OAuthToken getOAuthInfo(String username) {
//        String username = "wx" + mobile;
//        String password = "hit" + mobile;
        String password = userRegisterPassword;
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("grant_type", "password"));
        params.add(new BasicNameValuePair("username", username));
        params.add(new BasicNameValuePair("password", password));
        List<HashMap<String, Object>> headerMapList = new ArrayList<>();
        HashMap<String, Object> headerMap = new LinkedHashMap<>();
        headerMap.put("key", "Authorization");
        headerMap.put("value", "Basic ZmViczoxMjM0NTY=");
        headerMapList.add(headerMap);
        String resultStr = AuthUtils.doPostFormData(userOAuthUri, params, headerMapList);
        JSONObject resultJson = JSON.parseObject(resultStr);
        OAuthToken oAuthToken = resultJson.toJavaObject(OAuthToken.class);
        return (null != oAuthToken.getAccessToken()) ? oAuthToken : null;
    }

    @Override
    public boolean checkUsername(String username) {
        OAuthToken oAuthToken = this.getOAuthInfo(userRegister);
        String url = userSystemUri + "/check/" + username;
        List<HashMap<String, Object>> headerMapList = new ArrayList<>();
        HashMap<String, Object> headerMap = new LinkedHashMap<>();
        headerMap.put("key", "Authorization");
        headerMap.put("value", oAuthToken.getTokenType() + " " + oAuthToken.getAccessToken());
        headerMapList.add(headerMap);
        List<NameValuePair> params = new ArrayList<>();
        String result = AuthUtils.doGetFormData(url, params, headerMapList);
        return ("true".equals(result));
    }


    @Override
    public String addSysUser(UserDTO userDTO) {
        OAuthToken oAuthToken = this.getOAuthInfo(userRegister);
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("userId", userDTO.getUserId()));
        params.add(new BasicNameValuePair("username", userDTO.getUsername()));
        params.add(new BasicNameValuePair("password", userDTO.getPassword()));
        params.add(new BasicNameValuePair("mobile", userDTO.getMobile()));
        params.add(new BasicNameValuePair("sex", userDTO.getSex()));
        params.add(new BasicNameValuePair("status", userDTO.getStatus()));
        params.add(new BasicNameValuePair("roleId", userDTO.getRoleId()));
        params.add(new BasicNameValuePair("deptIds", userDTO.getDeptIds()));
        params.add(new BasicNameValuePair("deptIdsArr", userDTO.getDeptIdsArr()));
        List<HashMap<String, Object>> headerMapList = new ArrayList<>();
        HashMap<String, Object> headerMap = new LinkedHashMap<>();
        headerMap.put("key", "Authorization");
        headerMap.put("value", oAuthToken.getTokenType() + " " + oAuthToken.getAccessToken());
        headerMapList.add(headerMap);
        return AuthUtils.doPostFormData(userSystemUri, params, headerMapList);
    }
}

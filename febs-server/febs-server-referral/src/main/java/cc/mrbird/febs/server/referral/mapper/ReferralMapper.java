package cc.mrbird.febs.server.referral.mapper;

import cc.mrbird.febs.server.referral.entity.Referral;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 转诊列表 Mapper
 *
 * @author LiuDong
 * @date 2021-03-02 17:47:12
 */
public interface ReferralMapper extends BaseMapper<Referral> {

}

package cc.mrbird.febs.server.referral.entity;

import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;

/**
* 转诊列表 Entity
*
* @author LiuDong
* @date 2021-03-02 17:58:01
*/
@Data
@TableName("t_referral")
public class Referral {

    /**
     * 
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 患者姓名
     */
    @TableField("pat_name")
    @ApiModelProperty("患者姓名")
    private String patName;

    /**
     * 性别 01-男，02-女，03-未知
     */
    @TableField("gender")
    @ApiModelProperty("性别 01-男，02-女，03-未知")
    private String gender;

    /**
     * 患者年龄
     */
    @TableField("pat_age")
    @ApiModelProperty("患者年龄")
    private String patAge;

    /**
     * 患者电话
     */
    @TableField("pat_mobile")
    @ApiModelProperty("患者电话")
    private String patMobile;

    /**
     * 身份证号
     */
    @TableField("pat_id_no")
    @ApiModelProperty("身份证号")
    private String patIdNo;

    /**
     * 患者详细地址
     */
    @TableField("pat_address")
    @ApiModelProperty("患者详细地址")
    private String patAddress;

    /**
     * 初步诊断
     */
    @TableField("preliminary_diagnosis")
    @ApiModelProperty("初步诊断")
    private String preliminaryDiagnosis;

    /**
     * 病历资料
     */
    @TableField("clinical_data")
    @ApiModelProperty("病历资料")
    private String clinicalData;

    /**
     * 病历资料文件（文件名","分割）
     */
    @TableField("clinical_file")
    @ApiModelProperty("病历资料文件（文件名‘,’分割）")
    private String clinicalFile;

    /**
     * 选择医生ID
     */
    @TableField("choose_doctor_id")
    @ApiModelProperty("选择医生ID")
    private String chooseDoctorId;
	
    /**
     * 转诊目的
     */
    @TableField("transfer_purpose")
    @ApiModelProperty("转诊目的")
    private String transferPurpose;

    /**
     * I:住院，O:门诊, E:120急救
     */
    @TableField("trn_type")
    @ApiModelProperty("I:住院，O:门诊, E:120急救")
    private String trnType;

    /**
     * 转诊电话
     */
    @TableField("trn_tel")
    @ApiModelProperty("转诊电话")
    private String trnTel;

    /**
     * 申请状态
（0：暂存；1：发起；2：确认；3：拒绝；4：完成）
     */
    @TableField("apply_status")
    @ApiModelProperty("申请状态（0：暂存；1：发起；2：确认；3：拒绝；4：完成）")
    private String applyStatus;

    /**
     * 申请转入医院
     */
    @TableField("expect_transfer_hosp")
    @ApiModelProperty("申请转入医院")
    private String expectTransferHosp;

    /**
     * 期望转入时间
     */
    @TableField("expect_transfer_date")
    @ApiModelProperty("期望转入时间")
    private String expectTransferDate;

    /**
     * 申请医院
     */
    @TableField("apply_hosp")
    @ApiModelProperty("申请医院")
    private String applyHosp;

    /**
     * 申请科室
     */
    @TableField("apply_dept")
    @ApiModelProperty("申请科室")
    private String applyDept;

    /**
     * 申请医生
     */
    @TableField("apply_dr")
    @ApiModelProperty("申请医生")
    private String applyDr;

    /**
     * 申请医生id
     */
    @TableField("apply_dr_id")
    @ApiModelProperty("申请医生id")
    private String applyDrId;

    /**
     * 申请科室id
     */
    @TableField("apply_dept_id")
    @ApiModelProperty("申请科室id")
    private String applyDeptId;


    /**
     * 申请医生电话
     */
    @TableField("apply_doctor_mobile")
    @ApiModelProperty("申请医生电话")
    private String applyDoctorMobile;

    /**
     * 申请医院id
     */
    @TableField("apply_hosp_id")
    @ApiModelProperty("申请医院id")
    private String applyHospId;

    /**
     * 申请时间
     */
    @TableField("apply_date")
    @ApiModelProperty("申请时间")
    private String applyDate;

    /**
     * 确认转入时间
     */
    @TableField("confirm_transfer_date")
    @ApiModelProperty("确认转入时间")
    private String confirmTransferDate;

    /**
     * 确认转入主治医生
     */
    @TableField("confirm_transfer_dr")
    @ApiModelProperty("确认转入主治医生")
    private String confirmTransferDr;
	
	/**
     * 转入医生电话
     */
    @TableField("confirm_doctor_mobile")
    @ApiModelProperty("确认转入医生电话")
    private String confirmDoctorMobile;


    /**
     * 确认转入科室
     */
    @TableField("confirm_transfer_dept")
    @ApiModelProperty("确认转入科室")
    private String confirmTransferDept;

    /**
     * 审核备注信息
     */
    @TableField("review_remark")
    @ApiModelProperty("审核备注信息")
    private String reviewRemark;

    /**
     * 审核医院
     */
    @TableField("review_hosp")
    @ApiModelProperty("审核医院")
    private String reviewHosp;

    /**
     * 审核人
     */
    @TableField("reviewer")
    @ApiModelProperty("审核人")
    private String reviewer;

    /**
     * 审核日期
     */
    @TableField("review_date")
    @ApiModelProperty("审核日期")
    private String reviewDate;

    /**
     * 审核意见:Y/N
     */
    @TableField("review_pass")
    @ApiModelProperty("审核意见:Y/N")
    private String reviewPass;

    /**
     * 创建人ID
     */
    @TableField("created_by")
    @ApiModelProperty("创建人ID")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField("created_at")
    @ApiModelProperty("创建时间")
    private Date createdAt;

    /**
     * 更新人ID
     */
    @TableField("updated_by")
    @ApiModelProperty("更新人ID")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField("updated_at")
    @ApiModelProperty("更新时间")
    private Date updatedAt;

    /**
     * 删除状态 0-启用 1-删除
     */
    @TableField("delete_at")
    @ApiModelProperty("删除状态 0-启用 1-删除")
    private String deleteAt;

}
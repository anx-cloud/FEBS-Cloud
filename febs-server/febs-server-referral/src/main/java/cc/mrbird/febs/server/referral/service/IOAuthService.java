package cc.mrbird.febs.server.referral.service;

import cc.mrbird.febs.server.referral.dto.UserDTO;
import cc.mrbird.febs.server.referral.entity.OAuthToken;

/**
 * 微信用户信息表 Service接口
 *
 * @author MrBird
 * @date 2021-02-24 10:39:20
 */
public interface IOAuthService {

    OAuthToken getOAuthInfo(String mobile);

    boolean checkUsername(String username);

    String addSysUser(UserDTO userDTO);
}

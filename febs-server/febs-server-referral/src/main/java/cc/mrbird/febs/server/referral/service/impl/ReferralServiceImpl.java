package cc.mrbird.febs.server.referral.service.impl;

import cc.mrbird.febs.common.core.entity.FebsResponse;
import cc.mrbird.febs.common.core.utils.DateUtil;
import cc.mrbird.febs.server.referral.entity.Referral;
import cc.mrbird.febs.server.referral.entity.Doctor;
import cc.mrbird.febs.server.referral.mapper.DoctorMapper;
import cc.mrbird.febs.server.referral.mapper.ReferralMapper;
import cc.mrbird.febs.server.referral.service.IReferralService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import lombok.RequiredArgsConstructor;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cc.mrbird.febs.common.core.entity.QueryRequest;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 转诊列表 Service实现
 *
 * @author LiuDong
 * @date 2021-03-02 17:47:12
 */
@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ReferralServiceImpl extends ServiceImpl<ReferralMapper, Referral> implements IReferralService {

    private final ReferralMapper referralMapper;
	private final DoctorMapper doctorMapper;

    @Override
    public IPage<Referral> findReferrals(QueryRequest request, Referral referral) {
        LambdaQueryWrapper<Referral> queryWrapper = new LambdaQueryWrapper<>();
        // TODO 设置查询条件
        log.info("getChooseDoctorId:{}",referral.getChooseDoctorId());
        log.info("getApplyDrId:{}",referral.getApplyDrId());
        if(StringUtils.isNotBlank(referral.getChooseDoctorId()))
            queryWrapper.eq(Referral::getChooseDoctorId, referral.getChooseDoctorId());
        if(StringUtils.isNotBlank(referral.getApplyDrId()))
            queryWrapper.eq(Referral::getApplyDrId, referral.getApplyDrId());
        Page<Referral> page = new Page<>(request.getPageNum(), request.getPageSize());
        return this.page(page, queryWrapper);
    }

	/*
	@Override
    public List<ReferralDTO> findReferralDTOs(Referral referral) {
        List<Referral> referrals = this.findReferrals(referral);
		List<ReferralDTO> list = new ArrayList<ReferralDTO>();

		for(Referral referral : referrals) {
			Doctor doctor = this.doctorMapper.selectById(referral.getChooseDoctorId());
			list.
		}
    }*/

    @Override
    public List<Referral> findReferrals(Referral referral) {
        LambdaQueryWrapper<Referral> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
        log.info("getChooseDoctorId:{}",referral.getChooseDoctorId());
        log.info("getApplyDrId:{}",referral.getApplyDrId());
        if(StringUtils.isNotBlank(referral.getChooseDoctorId()))
            queryWrapper.eq(Referral::getChooseDoctorId, referral.getChooseDoctorId());
        if(StringUtils.isNotBlank(referral.getApplyDrId()))
            queryWrapper.eq(Referral::getApplyDrId, referral.getApplyDrId());

        // TODO 设置查询条件
        return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public FebsResponse createReferral(Referral referral) {
        referral.setId(UUID.randomUUID().toString());
        referral.setCreatedAt(new Date());

		// 确认接收医生信息
		Doctor doctor = new Doctor();
		doctor.setDoctorId(referral.getChooseDoctorId());
		List<Doctor> doctors = this.doctorMapper.selectDoctor(doctor);

		if(doctors != null && doctors.size() > 0) {
		   doctor = doctors.get(0);
		   referral.setExpectTransferHosp(doctor.getOrganization());
		   referral.setConfirmDoctorMobile(doctor.getMobile());
		   referral.setConfirmTransferDr(doctor.getDoctorName());
           referral.setConfirmTransferDept(doctor.getClinicName());
		}

		// 发起申请医生信息
		doctor = new Doctor();
		doctor.setDoctorId(referral.getApplyDrId());
		doctors = this.doctorMapper.selectDoctor(doctor);

		if(doctors != null && doctors.size() > 0) {
		   doctor = doctors.get(0);
		   referral.setApplyDoctorMobile(doctor.getMobile());
		   referral.setApplyDr(doctor.getDoctorName());
		   referral.setApplyDeptId(doctor.getClinicId());
           referral.setApplyDept(doctor.getClinicName());
           log.info("=======================insert====================");
            String nwoStr = DateUtil.getDateFormat(new Date(), "yyyy-MM-dd HH:mm:ss");
            referral.setApplyDate(nwoStr);
		}

        this.save(referral);
        return new FebsResponse().success(referral);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public FebsResponse updateReferral(Referral referral) {
        referral.setUpdatedAt(new Date());
        this.saveOrUpdate(referral);
        return new FebsResponse().success(referral);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteReferral(Referral referral) {
        LambdaQueryWrapper<Referral> wapper = new LambdaQueryWrapper<>();
        // TODO 设置删除条件
        wapper.eq(Referral::getId, referral.getId());
        this.remove(wapper);
    }
}

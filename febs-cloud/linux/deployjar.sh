if [[ $1 == "admin" ]] || [[ $1 == "all" ]]; then
cd /febs/linux/uploadjar
echo "cope febs-admin jar and refresh dockerbuild"
cp /febs/linux/uploadjar/febs-admin-2.2-RELEASE.jar /febs/febs-admin/febs-admin-2.2-RELEASE.jar
cd ..
cd /febs/febs-admin
docker build -t febs-admin .
cd ..
fi

if [[ $1 == "auth" ]] || [[ $1 == "all" ]] ; then
cd /febs/linux/uploadjar
echo "cope febs-auth jar"
cp /febs/linux/uploadjar/febs-auth-2.2-RELEASE.jar /febs/febs-auth/febs-auth-2.2-RELEASE.jar
cd ..
cd /febs/febs-auth
docker build -t febs-auth .
cd ..
fi

if [[ $1 == "gateway" ]] || [[ $1 == "all" ]] ; then
cd /febs/linux/uploadjar
echo "cope febs-gateway jar"
cp /febs/linux/uploadjar/febs-gateway-2.2-RELEASE.jar /febs/febs-gateway/febs-gateway-2.2-RELEASE.jar
cd ..
cd /febs/febs-gateway
docker build -t febs-gateway .
cd ..
fi

if [[ $1 == "generator" ]] || [[ $1 == "all" ]] ; then
cd /febs/linux/uploadjar
echo "cope febs-server-generator jar"
cp /febs/linux/uploadjar/febs-server-generator-2.2-RELEASE.jar /febs/febs-server-generator/febs-server-generator-2.2-RELEASE.jar
cd ..
cd /febs/febs-server-generator
docker build -t febs-server-generator .
cd ..
fi

if [[ $1 == "job" ]] || [[ $1 == "all" ]] ; then
cd /febs/linux/uploadjar
echo "cope febs-server-job jar"
cp /febs/linux/uploadjar/febs-server-job-2.2-RELEASE.jar /febs/febs-server-job/febs-server-job-2.2-RELEASE.jar
cd ..
cd /febs/febs-server-job
docker build -t febs-server-job .
cd ..
fi

if [[ $1 == "referral" ]] || [[ $1 == "all" ]] ; then
cd /febs/linux/uploadjar
echo "cope febs-server-referral jar"
cp /febs/linux/uploadjar/febs-server-referral-0.0.1-SNAPSHOT.jar /febs/febs-server-referral/febs-server-referral-0.0.1-SNAPSHOT.jar
cd ..
cd /febs/febs-server-referral
docker build -t febs-server-referral .
cd ..
fi

if [[ $1 == "system" ]] || [[ $1 == "all" ]] ; then
cd /febs/linux/uploadjar
echo "cope febs-server-system jar"
cp /febs/linux/uploadjar/febs-server-system-2.2-RELEASE.jar /febs/febs-server-system/febs-server-system-2.2-RELEASE.jar
cd ..
cd /febs/febs-server-system
docker build -t febs-server-system .
cd ..
fi

if [[ $1 == "test" ]] || [[ $1 == "all" ]] ; then
cd /febs/linux/uploadjar
echo "cope febs-server-test jar"
cp /febs/linux/uploadjar/febs-server-test-2.2-RELEASE.jar /febs/febs-server-test/febs-server-test-2.2-RELEASE.jar
cd ..
cd /febs/febs-server-test
docker build -t febs-server-test .
cd ..
fi

if [[ $1 == "manager" ]] || [[ $1 == "all" ]] ; then
cd /febs/linux/uploadjar
echo "cope tx-manager jar"
cp /febs/linux/uploadjar/febs-tx-manager-2.2-RELEASE.jar /febs/febs-tx-manager/febs-tx-manager-2.2-RELEASE.jar
cd ..
cd /febs/febs-tx-manager
docker build -t febs-tx-manager .
cd ..
fi

if [ $1 == "restart" ] ; then
echo "docker-compose 重启.............."
cd /febs/febs-cloud/
docker-compose restart
fi

if [ $1 == "down" ] ; then
echo "docker-compose down.............."
cd /febs/febs-cloud/
docker-compose down
fi

if [ $1 == "up" ] ; then
echo "docker-compose up.............."
cd /febs/febs-cloud/
docker-compose up -d
fi


if [ $1 == "reup" ] ; then
echo "docker-compose down.............."
cd /febs/febs-cloud/
docker-compose down

sleep 3

echo "docker-compose up.............."
cd /febs/febs-cloud/
docker-compose up -d
fi


exit 0
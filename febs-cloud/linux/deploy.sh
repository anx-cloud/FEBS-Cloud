
echo "一.开始新建文件夹..."

mkdir -p /febs
echo "1.新建febs文件夹"
cd /febs
mkdir -p /febs/febs-cloud
echo "2.新建febs-cloud文件夹"
mkdir -p /febs/febs-auth
echo "3.新建febs-auth文件夹"
mkdir -p /febs/febs-gateway
echo "4.新建febs-gateway文件夹"
mkdir -p /febs/febs-server-generator
echo "5.新建febs-server-generator文件夹"
mkdir -p /febs/febs-server-job
echo "6.新建febs-server-job文件夹"
mkdir -p /febs/febs-server-referral
echo "7.新建febs-server-referral文件夹"
mkdir -p /febs/febs-server-system
echo "8.新建febs-server-system文件夹"
mkdir -p /febs/febs-server-test
echo "9.新建febs-server-test文件夹"
mkdir -p /febs/febs-tx-manager
echo "10.新建febs-tx-manager文件夹"
mkdir -p /febs/nacos
echo "11.新建nacos文件夹"
mkdir -p /febs/FEBS-Cloud-Web
echo "12.新建FEBS-Cloud-Web文件夹"
mkdir -p /febs/febs-admin
echo "13.新建febs-admin文件夹"

mkdir -p /febs/linux/uploadjar
echo "14.新建uploadjar文件夹"


echo "二.开始拷贝Dockerfile. docker-compose.yml.."

echo "2.1.开始拷贝febs-auth Dockerfile..."
mv /febs/linux/docker/auth-Dockerfile /febs/febs-auth/Dockerfile

echo "2.2.开始拷贝febs-gateway Dockerfile..."
mv /febs/linux/docker/gateway-Dockerfile /febs/febs-gateway/Dockerfile

echo "2.3.开始拷贝febs-server-generator Dockerfile..."
mv /febs/linux/docker/generator-Dockerfile /febs/febs-server-generator/Dockerfile

echo "2.4.开始拷贝febs-server-job Dockerfile..."
mv /febs/linux/docker/job-Dockerfile /febs/febs-server-job/Dockerfile

echo "2.5.开始拷贝febs-server-referral Dockerfile..."
mv /febs/linux/docker/referral-Dockerfile /febs/febs-server-referral/Dockerfile

echo "2.6.开始拷贝febs-server-system Dockerfile..."
mv /febs/linux/docker/system-Dockerfile /febs/febs-server-system/Dockerfile

echo "2.7.开始拷贝febs-server-test Dockerfile..."
mv /febs/linux/docker/test-Dockerfile /febs/febs-server-test/Dockerfile

echo "2.8.开始拷贝febs-admin Dockerfile..."
mv /febs/linux/docker/admin-Dockerfile /febs/febs-admin/Dockerfile

echo "2.9.开始拷贝febs-tx-manager Dockerfile..."
mv /febs/linux/docker/tx-Dockerfile /febs/febs-tx-manager/Dockerfile

echo "2.10.开始拷贝docker-compose.yml ..."
mv /febs/linux/docker/docker-compose.yml /febs/febs-cloud/docker-compose.yml
